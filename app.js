// 1) Опишіть своїми словами різницю між функціями setTimeout() і setInterval().
//      setTimeout дозволяє викликати функцію один раз через певний проміжок часу.
//      setInterval дозволяє викликати функцію регулярно, повторюючи виклик через певний проміжок часу.
// 2) Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
//    setTimeout(func, 0) планує виклик func настільки швидко, наскільки це можливо. Але воно буде виконане лише
//    після завершення виконання поточного коду.
// 3) Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
//    clearTimeout() перериває таймер і встановлена функція вже не викликається. Його використовують для відчистки
//    таймерів, якщо вони більше не потрібні.


const images = document.querySelectorAll(".image-to-show");
const btnStop = document.createElement("button");
btnStop.textContent = "Stop";
document.body.append(btnStop);

const btnContinue = document.createElement("button");
btnContinue.textContent = "Resume show";
btnStop.after(btnContinue);
btnContinue.addEventListener("click", () => {
    startShow();
});

const startShow = function () {
    const show = setInterval(() => {

            for (let i = 0; i < images.length; i++) {
                if (images[i].classList.contains("visible") && i === 3) {
                    images[i].classList.remove("visible");
                    images[0].classList.add("visible");
                    break
                } else if (images[i].classList.contains("visible") && i < 3) {
                    images[i + 1].classList.add("visible");
                    images[i].classList.remove("visible");
                    break
                }
            }
        },
        2000);
    btnContinue.disabled = true;
    btnStop.addEventListener("click", () => {
        clearInterval(show);
        btnContinue.disabled = false;
    });
};

startShow();


